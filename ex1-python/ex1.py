import pandas as pd
import numpy as np
from warmUpExercise import warmupexercise 
from plotData import plotData
from computeCost import computeCost
from gradientDescent import gradientDescent
import matplotlib.pyplot as plt

#Part I: Basic Function
print('Running warmUpExercise ... \n')
print('5x5 Identity Matrix: \n')
print(warmupexercise())

#Part II: Plotting
print('Plotting Data ...\n')
data = pd.read_csv('ex1data1.txt',header=None).values
X = data[:,0:-1]
y = data[:,-1].reshape(97,1)
m = len(y)
#plotData(X,y)
plt.plot(X,y,"rx",markersize=10)

#Part III: Cost and Gradient descent
X = np.append(np.ones((m,1),dtype=int),X,axis=1)
theta = (np.zeros((2,1)))
iterations = 1500
a = 0.01

print('\nTesting the cost function ...\n')
J = computeCost(X, y, theta)
print('With theta = [0,0]\nCost computed = {}\n'.format(J))
print('Expected cost value (approx) 32.07\n')

J = computeCost(X, y,np.array([-1,2]).reshape(2,1))
print('\nWith theta = [-1,2]\nCost computed = {}\n'.format(J))
print('Expected cost value (approx) 54.24\n')

print('\nRunning Gradient Descent ...\n')
theta = gradientDescent(X, y, theta, a, iterations)
print('Theta found by gradient descent:\n')
print('{}\n'.format(theta))
print('Expected theta values (approx)\n')
print(' -3.6303\n  1.1664\n\n')

plt.plot(X[:,1],np.dot(X,theta))
plt.show()

predict1 = np.dot([1, 3.5],theta)
print('For population = 35,000, we predict a profit of {}\n'.format(predict1*10000))
predict2 = np.dot([1, 7],theta)
print('For population = 70,000, we predict a profit of {}\n'.format(predict2*10000))