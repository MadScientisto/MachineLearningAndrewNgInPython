import numpy as np
from computeCost import computeCost

def gradientDescent(X,y,theta,alpha,iterations):
    m = len(y)
    C_history = []
    for i in range(0,iterations):
        loss = np.subtract(np.dot(X,theta),y)
        gradient = np.dot(X.T,loss) / m
        theta = theta - alpha * gradient
        C_history.append(computeCost(X, y, theta))
    
    print(C_history)
    return theta


