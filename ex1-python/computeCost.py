import numpy as np

def computeCost(x,y,theta):
    m = len(y)
    predict = np.dot(x,theta)
    loss = np.subtract(predict,y)
    cost = sum(np.square(loss))/(2*m)
    return cost
