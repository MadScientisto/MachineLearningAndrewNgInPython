import numpy as np
from sigmoid import sigmoid

def predict(theta,X):
    h = sigmoid(np.dot(X,theta))
    p = np.array(list(map(lambda x: 1 if x >= 0.5 else 0,h))).reshape((X.shape[0],1))
    return p