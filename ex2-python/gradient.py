import numpy as np
from sigmoid import sigmoid

def gradient(theta,X,y):
    m = len(y)
    
    h = sigmoid(np.dot(X,theta))
    
    error = np.subtract(h,y)
    grad = np.dot(X.T,error) / m

    return grad.flatten()