import numpy as np 
from sigmoid import sigmoid

def gradientReg(theta,X,y,l):
    m = len(y)
    theta = np.reshape(theta,(X.shape[1],1))
   
    h = sigmoid(np.dot(X,theta))
    error = np.subtract(h,y)
    grad = np.round_(np.dot(X.T,error) / m, decimals=4)

    regGrad = np.add(grad,theta*(l/m))
    regGrad[0,0] = grad[0,0]
    
    return regGrad.flatten()