import pandas as pd
import numpy as np
import scipy.optimize as op
import matplotlib.pyplot as plt
import matplotlib.lines as mlines
from plotData import plotData
from mapFeature import mapFeature
from gradientReg import gradientReg
from costFunctionReg import costFunctionReg 
from plotDecisionBoundary import plotDecisionBoundary
from predict import predict
from sigmoid import sigmoid


data = pd.read_csv("ex2data2.txt",header=None).values
X = data[:,0:2]
y = data[:,-1:]

plotData(X,y)
plt.xlabel('Microchip Test 1')
plt.ylabel('Microchip Test 2')
circle = mlines.Line2D([], [], color='red', marker='o', markersize=10, label='y=0',linestyle='')
cross  = mlines.Line2D([], [], color='blue', marker='+', markersize=10, label='y=1',linestyle='')
plt.legend(handles=[circle,cross])
plt.show()

#Part 1: Regularized Logistic Regression

X = mapFeature(X[:,0:1], X[:,1:])
initial_theta = np.zeros((X.shape[1],1))
l = 1

cost = costFunctionReg(initial_theta,X,y,l)
grad = gradientReg(initial_theta,X,y,l)

print('Cost at initial theta (zeros): {}\n'.format(cost))
print('Expected cost (approx): 0.693\n')
print('Gradient at initial theta (zeros) - first five values only:\n')
print(' {} \n'.format(grad[0:5]))
print('Expected gradients (approx) - first five values only:\n')
print(' 0.0085\n 0.0188\n 0.0001\n 0.0503\n 0.0115\n')

test_theta = np.ones((X.shape[1],1))

cost = costFunctionReg(test_theta, X, y, 10)
grad = gradientReg(test_theta,X,y,10)

print('\nCost at test theta (with lambda = 10): {}\n'.format(cost))
print('Expected cost (approx): 3.16\n')
print('Gradient at test theta - first five values only:\n')
print(' {} \n'.format(grad[0:5]))
print('Expected gradients (approx) - first five values only:\n')
print(' 0.3460\n 0.1614\n 0.1948\n 0.2269\n 0.0922\n')

#Part 2: Regularization and Accuracies
initial_theta = np.zeros((X.shape[1], 1))


for i, l in enumerate([0, 1, 10, 100]):
    plotData(X,y)
    
    res = op.minimize(costFunctionReg, initial_theta, args=(X, y, l), method=None, jac=gradientReg, options={'maxiter':3000})
    opt_theta = res.x
    
    p = predict(opt_theta, X)
    acc = 1 - np.sum(np.square((np.subtract(p,y)))) / len(y)   

    plt.xlabel('Microchip Test 1')
    plt.ylabel('Microchip Test 2')
    circle = mlines.Line2D([], [], color='red', marker='o', markersize=10, label='y=0',linestyle='')
    cross  = mlines.Line2D([], [], color='blue', marker='+', markersize=10, label='y=1',linestyle='')
    plt.legend(handles=[circle,cross])

    x1_min, x1_max = X[:,1].min(), X[:,1].max(),
    x2_min, x2_max = X[:,2].min(), X[:,2].max(),
    xx1, xx2 = np.meshgrid(np.linspace(x1_min, x1_max), np.linspace(x2_min, x2_max))
    h = sigmoid(mapFeature(xx1.ravel().reshape((2500,1)), xx2.ravel().reshape(2500,1)).dot(opt_theta))
    h = h.reshape(xx1.shape)
    plt.contour(xx1, xx2, h, [0.5], linewidths=1, colors='g')
    plt.title('Train accuracy {}% with Lambda = {}'.format(np.round(acc*100, decimals=2), l))
    
    plt.show()