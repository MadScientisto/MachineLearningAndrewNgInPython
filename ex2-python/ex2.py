import pandas as pd
import numpy as np
import scipy.optimize as op
import matplotlib.pyplot as plt
import matplotlib.lines as mlines
from plotData import plotData
from costFunction import costFunction
from gradient import gradient
from plotDecisionBoundary import plotDecisionBoundary
from sigmoid import sigmoid
from predict import predict

data = pd.read_csv("ex2data1.txt",header=None).values
X = data[:,0:2]
y = data[:,-1:]

#Part 1: Plotting

print('Plotting data with + indicating (y = 1) examples and o indicating (y = 0) examples.\n')

plotData(X,y)
plt.xlabel('Exam 1 score')
plt.ylabel('Exam 2 score')
circle = mlines.Line2D([], [], color='red', marker='o', markersize=10, label='Not admitted',linestyle='')
cross  = mlines.Line2D([], [], color='blue', marker='+', markersize=10, label='Admitted',linestyle='')
plt.legend(handles=[circle,cross])
plt.show()

#Part 2: Compute Cost and Gradient

m, n = X.shape
X = np.append(np.ones((m,1),dtype=float),X,axis=1)
initial_theta = np.zeros((n+1,1),dtype=float)

cost = costFunction(initial_theta, X, y)
grad = gradient(initial_theta, X, y)


print('Cost at initial theta (zeros): {}\n'.format(cost))
print('Expected cost (approx): 0.693\n')
print('Gradient at initial theta (zeros): \n')
print(' {} \n'.format(grad))
print('Expected gradients (approx):\n -0.1000\n -12.0092\n -11.2628\n')

test_theta = np.array([-24,0.2,0.2]).reshape(3,1)
cost = costFunction(test_theta, X, y)
grad = gradient(test_theta, X, y)

print('\nCost at test theta: {}\n'.format(cost))
print('Expected cost (approx): 0.218\n')
print('Gradient at test theta: \n')
print(' {}\n'.format(grad))
print('Expected gradients (approx):\n 0.043\n 2.566\n 2.647\n')

#Part 3: Optimizing using scipy.optimize

res = op.minimize(costFunction, initial_theta, args=(X,y), method='TNC', options={'maxiter':400})
opt_theta = res.x
cost = costFunction(opt_theta,X,y)

print('Cost at theta found by fminunc: {}\n'.format(cost))
print('Expected cost (approx): 0.203\n')
print('theta: \n')
print(' {} \n'.format(opt_theta))
print('Expected theta (approx):\n')
print(' -25.161\n 0.206\n 0.201\n')

plotDecisionBoundary(opt_theta, X, y)
plt.show()

#Part 4: Predict and Accuracies

prob = sigmoid(np.dot([1,45,85],opt_theta))
print('For a student with scores 45 and 85, we predict an admission probability of {}\n'.format(prob))
print('Expected value: 0.775 +/- 0.002\n\n')

p = predict(opt_theta, X)
acc = 1 - np.sum(np.square((np.subtract(p,y)))) / m
print('Train Accuracy: {}\n'.format(acc*100))
print('Expected accuracy (approx): 89.0\n')
print('\n')


