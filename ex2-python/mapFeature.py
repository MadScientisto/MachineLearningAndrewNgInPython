import numpy as np

def mapFeature(x1,x2):
    out = np.ones(x1.shape)

    for i in range(1,7):
        for j in range(0,i+1):
            a = np.power(x1, i-j)
            b = np.power(x2, j)
            c = np.multiply(a,b)
            out = np.append(out,c,axis=1)
    
    return out
    