import matplotlib.pyplot as plt
import matplotlib.lines as mlines

def plotData(X,y):
    markers = map(lambda y: ("+","b") if y == 1 else ("o","r"),y )
    f = 2 - X.shape[1] 
    for x1,x2,m in zip(X[:,f-1],X[:,f],markers):
        plt.scatter(x1,x2,marker=m[0],color=m[1],s=50)

    return