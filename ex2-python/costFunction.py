import numpy as np
from sigmoid import sigmoid

def costFunction(theta,X,y):
    m = len(y)
   
    h = sigmoid(np.dot(X,theta))
    cost = (-np.dot(y.T,np.log(h))-np.dot((1-y.T),np.log(1-h)))/m
    
    return cost