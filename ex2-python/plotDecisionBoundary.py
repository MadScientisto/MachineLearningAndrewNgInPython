import matplotlib.pyplot as plt
import numpy as np
from plotData import plotData
from predict import predict
from sigmoid import sigmoid

def plotDecisionBoundary(theta, X, y):

    plotData(X,y)
    theta = np.reshape(theta,(X.shape[1],1))

    x1_min, x1_max = X[:,1].min(), X[:,1].max()
    x2_min, x2_max = X[:,2].min(), X[:,2].max()
    xx1, xx2 = np.meshgrid(np.linspace(x1_min, x1_max), np.linspace(x2_min, x2_max))
    h = sigmoid(np.c_[np.ones((xx1.ravel().shape[0],1)), xx1.ravel(), xx2.ravel()].dot(theta))
    h = h.reshape(xx1.shape)
    plt.contour(xx1, xx2, h, [0.5], linewidths=1, colors='b')

    return

    

