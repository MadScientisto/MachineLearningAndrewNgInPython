import numpy as np 

def featureNormalize(X):
    
    for i in range(1,X.shape[1]):
        mu = np.mean(X[:,i])
        sigma = np.std(X[:,i])
        X[:,i] = (X[:,i] - mu) / sigma

    return X
