import numpy as np
import matplotlib.pyplot as plt
from scipy.io import loadmat
from linearRegCostFunction import linearRegCostFunction
from trainLinearReg import trainLinearReg
from learningCurve import learningCurve
from polyFeatures import polyFeatures
from featureNormalize import featureNormalize
from plotFit import plotFit

#Loading and Visualising data
data = loadmat('ex5data1.mat')

X = data['X']
X = np.append(np.ones((X.shape[0],1)),X,axis=1)
y = data['y']

Xval = data['Xval']
Xval = np.append(np.ones((Xval.shape[0],1)),Xval,axis=1)
yval = data['yval']

Xtest = data['Xtest']
Xtest = np.append(np.ones((Xtest.shape[0],1)),Xtest,axis=1)
ytest = data['ytest']

plt.scatter(X[:,1:],y,c='r',marker='+')
plt.xlabel('Change in water level (x)')
plt.ylabel('Water flowing out of the dam (y)')
plt.show()

#Regularized Linear Regression Cost and Gradient

init_theta = np.ones((2,1))
cost,grad = linearRegCostFunction(init_theta, X, y, 1)
print('Cost at theta = [1,1]: {}\n(this value should be about 303.993192)\n'.format(cost))
print('Gradient at theta = [1,1]:  [{},{}] \n(this value should be about [-15.303016,598.250744])\n'.format(grad[0], grad[1]))

#Train Linear Regression

opt_theta = trainLinearReg(init_theta,X,y,1)
pred = X.dot(opt_theta)
plt.scatter(X[:,1:],y,c='r',marker='+')
plt.plot(X[:,1:],pred, color='blue',linewidth=2)
plt.show()
plt.clf()

#Learning Curve for Linear Regression

l = 0
m = X.shape[0]
error_train, error_val = learningCurve(X,y,Xval,yval,l)
plt.plot(range(1,m+1),error_train,color='blue',label='Train',linewidth=2)
plt.plot(range(1,m+1),error_val,color='red',label='Cross Validation',linewidth=2)
plt.xlabel('Number of training examples')
plt.ylabel('Error')
plt.legend()
plt.show()
plt.clf()

print('# Training Examples\tTrain Error\tCross Validation Error\n')
for i in range(0,m):
    print('  \t{}\t\t{}\t{}\n'.format(i+1, error_train[i], error_val[i]))

#Feature Mapping for Polynomial Regression

p = 8

X_poly = featureNormalize(polyFeatures(X,p))
X_poly_test = featureNormalize(polyFeatures(Xtest,p))
X_poly_val = featureNormalize(polyFeatures(Xval,p))

print('Normalized Training Example 1:\n')
print('  {}  \n'.format(X_poly[1,:]))

#Learning Curve for Polynomial Regression

l = 0
init_theta = np.ones((X_poly.shape[1],1))
opt_theta = trainLinearReg(init_theta,X_poly,y,l)

plt.scatter(X[:,1:],y,c='r',marker='+')
plotFit(opt_theta,X,p)
plt.xlabel('Change in water level (x)')
plt.ylabel('Water flowing out of the dam (y)')
plt.title('Polynomial Regression Fit (lambda = {})'.format(l))
plt.show()
plt.clf()

m = X_poly.shape[0]
error_train, error_val = learningCurve(X_poly,y,X_poly_val,yval,l)
plt.plot(range(1,m+1),error_train,color='blue',label='Train',linewidth=2)
plt.plot(range(1,m+1),error_val,color='red',label='Cross Validation',linewidth=2)
plt.xlabel('Number of training examples')
plt.ylabel('Error')
plt.legend()
plt.show()
plt.clf()

print('Polynomial Regression (lambda = {})\n\n'.format(l))
print('# Training Examples\tTrain Error\tCross Validation Error\n')
for i in range(0,m):
    print('  \t{}\t\t{}\t{}\n'.format(i+1, error_train[i], error_val[i]))