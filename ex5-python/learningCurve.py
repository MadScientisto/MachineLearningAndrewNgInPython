import numpy as np
from linearRegCostFunction import linearRegCostFunction
from trainLinearReg import trainLinearReg

def learningCurve(X,y,Xval,yval,l):
    m = X.shape[0]
    error_train = []
    error_val = []
    init_theta = np.ones((X.shape[1],1))

    for i in range(1,m+1):
        opt_theta = trainLinearReg(init_theta,X[0:i,:],y[0:i,:],l)
        
        error_train.append(linearRegCostFunction(opt_theta,X[0:i,:],y[0:i,:],0)[0])
        error_val.append(linearRegCostFunction(opt_theta,Xval,yval,0)[0])

    

    return error_train,error_val
    