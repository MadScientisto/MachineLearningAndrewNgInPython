import numpy as np

def linearRegCostFunction(theta,X,y,l):
    m = len(y)
    theta = theta.reshape((X.shape[1]),1)
    
    h = np.dot(X,theta)
    cost = np.sum(np.square(h - y))/(2*m) + (l/(2*m))*np.sum(np.square(theta))
    grad = np.dot(X.T,h-y)/m + (l/m)*theta
    grad[0,0] = grad[0,0] - (l/m)*theta[0,0]

    return cost,grad.flatten()