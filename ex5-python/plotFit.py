import numpy as np
import matplotlib.pyplot as plt
from polyFeatures import polyFeatures
from featureNormalize import featureNormalize

def plotFit(opt_theta,X,p):

    x_min, x_max = (X[:,1].min())-15, (X[:,1].max()+10)
    XX = np.linspace(x_min,x_max)
    XX = np.append(np.ones((len(XX),1)),XX.reshape(len(XX),1),axis=1)
    X_poly = polyFeatures(X,p)
    XX_poly = polyFeatures(XX,p)

    for i in range(1,X_poly.shape[1]):
        mu = np.mean(X_poly[:,i])
        sigma = np.std(X_poly[:,i])
        XX_poly[:,i] = (XX_poly[:,i] - mu) / sigma

    
    pred = XX_poly.dot(opt_theta)

    plt.plot(XX[:,1],pred,color='blue',linewidth=2)

