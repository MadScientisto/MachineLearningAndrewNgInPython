import numpy as np

def polyFeatures(X,p):
    
    for i in range(2,p+1):
        X = np.append(X,(X[:,1]**i).reshape(X.shape[0],1),axis=1)

    return X