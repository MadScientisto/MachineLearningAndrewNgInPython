import scipy.optimize as op
from linearRegCostFunction import linearRegCostFunction

def trainLinearReg(init_theta,X,y,l):
    res = op.minimize(linearRegCostFunction,init_theta,args=(X,y,l),method='CG',jac=True,options={'maxiter':200})
    return res.x.reshape(X.shape[1],1)