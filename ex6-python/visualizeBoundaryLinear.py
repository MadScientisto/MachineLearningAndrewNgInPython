import numpy as np
import matplotlib.pyplot as plt

def visualizeBoundaryLinear(X,y,model):
    posx1 = []
    posx2 = []
    negx1 = []
    negx2 = []
    for i in range(0,X.shape[0]):
        if y[i] == 1:
            posx1.append(X[i,0])
            posx2.append(X[i,1])
        else:
            negx1.append(X[i,0])
            negx2.append(X[i,1])
    
    plt.scatter(posx1,posx2,c='black',marker='+')
    plt.scatter(negx1,negx2,c='yellow',marker='o')

    x1_min, x1_max = X[:,0].min(), X[:,0].max()
    x2_min, x2_max = X[:,1].min(), X[:,1].max()
    num = 500
    xx1, xx2 = np.meshgrid(np.linspace(x1_min, x1_max,num=num), np.linspace(x2_min, x2_max,num=num))
    p = model.predict(np.append(xx1.reshape((num**2,1)),xx2.reshape((num**2,1)),axis=1))
    p = p.reshape(xx1.shape)
    plt.contour(xx1,xx2,p,[0.5],linewidths=1, colors='b')
    plt.show()