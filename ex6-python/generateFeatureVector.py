import numpy as np
from nltk.stem import PorterStemmer
from nltk.tokenize import word_tokenize 
import re
import string

def processEmail(email,vocab):
    
    #Convert to lowercase
    email = email.lower()
    #Remove html tags
    email = re.sub(r'<\w*>',' ',email)
    #Normalize numbers
    email = re.sub(r'\d+',' number ',email)
    #Normalize URLs
    email = re.sub(r'(http|https)://\S*',' httpaddr ',email)
    #Normalize email addresses
    email = re.sub(r'\S+@\S+\.\S+',' emailaddr ',email)
    #Normalize dollars
    email = re.sub(r'\$+',' dollar ',email)

    tokens = list(filter(lambda x: x not in string.punctuation,word_tokenize(email)))

    ps = PorterStemmer()
    stem_tokens = [ps.stem(t) for t in tokens]
    
    word_indices = [vocab.index(w) for w in stem_tokens if w in vocab]
    
    return word_indices
 

def emailFeatures(word_indices,n):
    features = np.zeros((n,1))
    for w in word_indices:
        features[w,0] = 1
    return features 