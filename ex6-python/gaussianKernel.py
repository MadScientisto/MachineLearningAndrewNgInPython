import numpy as np

def gaussianKernel(x1,x2,sigma):
    sim = np.exp(-np.sum(np.square(x1-x2))/(2*sigma**2))
    return sim