import numpy as np
from sklearn import svm
import matplotlib.pyplot as plt
from scipy.io import loadmat
from plotData import plotData
from visualizeBoundaryLinear import visualizeBoundaryLinear
from gaussianKernel import gaussianKernel

#Loading and Visualizing Data

print('Loading and Visualizing Data ...\n')

data1 = loadmat('ex6data1.mat')
X = data1['X']
y = data1['y']

plotData(X,y)

#Training Linear SVM

print('\nTraining Linear SVM ...\n')

C = 1
model = svm.SVC(C,kernel='linear',tol=1e-3)
model.fit(X,y.ravel())

visualizeBoundaryLinear(X,y,model)

#Implementing Gaussian Kernel

print('\nEvaluating the Gaussian Kernel ...\n')

x1 = np.array([1,2,1])
x2 = np.array([0,4,-1])
sigma = 2
sim = gaussianKernel(x1, x2, sigma)

print('Gaussian Kernel between x1 = [1,2,1], x2 = [0,4,-1], sigma = {} :' \
        '\n\t%{}\n(for sigma = 2, this value should be about 0.324652)\n'.format(sigma, sim))

#Visualizing Dataset 2

data2 = loadmat('ex6data2.mat')
X = data2['X']
y = data2['y']

plotData(X,y)

#Training SVM with RBF Kernel (Dataset 2)

print('\nTraining SVM with RBF Kernel...\n')
C = 1 
sigma = 0.1
gamma = lambda sigma: 1/(2*(sigma**2))

model2 = svm.SVC(C,tol=1e-3,gamma=gamma(sigma))
model2.fit(X,y.ravel())

visualizeBoundaryLinear(X,y,model2)


#Visualizing Dataset 3

data3 = loadmat('ex6data3.mat')
X = data3['X']
y = data3['y']
Xval = data3['Xval']
yval = data3['yval']

plotData(X,y)

#Training SVM with RBF Kernel (Dataset 3)

param_values = [0.01,0.03,0.1,0.3,1,3,10,30]
opt_C = 0
opt_sigma = 0
max_acc = 0
for c in param_values:
    for s in param_values:
        model3 = svm.SVC(c,tol=1e-3,gamma=gamma(s))
        model3.fit(X,y.ravel())
        p = model3.predict(Xval)
        acc = sum([1 if p == y  else 0 for p,y in zip(p,yval)]) / len(p)
        if acc > max_acc:
            opt_C = c
            opt_sigma = s
            max_acc = acc

print('Maximum accuracy of {}% was achieved for C:{} and sigma:{}'.format(max_acc*100,opt_C,opt_sigma))

opt_model3 = model3 = svm.SVC(opt_C,tol=1e-3,gamma=gamma(opt_sigma))
opt_model3.fit(X,y.ravel())

visualizeBoundaryLinear(X,y,opt_model3)







