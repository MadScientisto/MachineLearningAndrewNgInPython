import matplotlib.pyplot as plt
import numpy as np

def plotData(X,y):
    posx1 = []
    posx2 = []
    negx1 = []
    negx2 = []
    for i in range(0,X.shape[0]):
        if y[i] == 1:
            posx1.append(X[i,0])
            posx2.append(X[i,1])
        else:
            negx1.append(X[i,0])
            negx2.append(X[i,1])
    
    plt.scatter(posx1,posx2,c='black',marker='+')
    plt.scatter(negx1,negx2,s=20,c='yellow',marker='o',edgecolors ='black')
    plt.show()
    return


