import numpy as np
from scipy.io import loadmat
from sklearn import svm
import generateFeatureVector as gfv


#Email Preprocessing
vocab = [word.split()[1] for word in open('vocab.txt','r').readlines()]

file_obj = open('emailSample1.txt','r')
contents = file_obj.read()
word_indices = gfv.processEmail(contents,vocab)

print('Word Indices: \n')
print(' {}'.format(word_indices))
print('\n\n')

#Feature Extraction

features = gfv.emailFeatures(word_indices,len(vocab))

print('Length of feature vector: {}\n'.format(len(features)))
print('Number of non-zero entries: {}\n'.format(np.sum(features)))

#Train Linear SVM for Spam Classification

data_test = loadmat('spamTest.mat')
data_train = loadmat('spamTrain.mat')

X = data_train['X']
y = data_train['y']
Xtest = data_test['Xtest']
ytest = data_test['ytest']

print('\nTraining Linear SVM (Spam Classification)\n')
print('(this may take 1 to 2 minutes) ...\n')

model = svm.SVC(kernel='linear')
model.fit(X,y.ravel())

acc = model.score(Xtest,ytest)
print('Training Accuracy: {}%\n'.format(acc*100))

#Top Predictors of Spam

weights = model.coef_.ravel()
sort_ind = np.argsort(weights)[::-1]


print('\nTop predictors of spam: \n')
for i in range(15):
    w_ind = sort_ind[i]
    print(' {}: {} - ({}) \n'.format(i+1,vocab[w_ind], weights[w_ind]))






