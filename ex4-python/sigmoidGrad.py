import numpy as np
from sigmoid import sigmoid

def sigmoidGrad(z):
    return(sigmoid(z)*(1-sigmoid(z)))