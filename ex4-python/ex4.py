import numpy as np
import matplotlib.pyplot as plt
from scipy.io import loadmat
from scipy.optimize import minimize
from nnCostFunction import nnCostFunction
from sigmoidGrad import sigmoidGrad
from randomInitializeWeights import randomInitializeWeights
from predict import predict

#Load Data and Weights
data = loadmat('ex4data1.mat')
weights = loadmat('ex4weights.mat')

X = np.append(np.ones((data['X'].shape[0],1)),data['X'],axis=1)
y = data['y']
theta1 = weights['Theta1']
theta2 = weights['Theta2']
nn_params = np.append(theta1.ravel(),theta2.ravel())


#Testing Cost Function without Regularization
print('\nFeedforward Using Neural Network ...\n')

l = 0
input_layer_size  = 400
hidden_layer_size = 25
num_labels = 10
J = nnCostFunction(nn_params, input_layer_size, hidden_layer_size, num_labels, X, y, l)

print('Cost at parameters (loaded from ex4weights): {} \n(this value should be about 0.287629)\n'.format(J[0]))

#Testing Cost Function with Regularization

l = 1

J = nnCostFunction(nn_params, input_layer_size, hidden_layer_size, num_labels, X, y, l)

print('Cost at parameters (loaded from ex4weights): {} \n(this value should be about 0.383770)\n'.format(J[0]))

#Testing sigmoidGrad

g = sigmoidGrad(np.array([-1,-0.5,0,0.5,100]))
print('Sigmoid gradient evaluated at [-1 -0.5 0 0.5 1]:\n  ')
print('{} '.format(g))
print('\n\n')

#Initializing Pameters
print ('\nInitializing Neural Network Parameters ...\n')

init_theta1 = randomInitializeWeights(input_layer_size,hidden_layer_size)
init_theta2 = randomInitializeWeights(hidden_layer_size,num_labels)

init_nn_params = np.append(init_theta1.ravel(),init_theta2.ravel())

#Training NN
l = 1 

def reduced_cost_func(p):
    
    return nnCostFunction(p,input_layer_size,hidden_layer_size,num_labels,
                          X,y,l)

res = minimize(reduced_cost_func,
                   init_nn_params,
                   method="CG",
                   jac=True,
                   options={'maxiter':50, "disp":True})

nn_params = res.x

theta1 = nn_params[:(hidden_layer_size * 
                    (input_layer_size + 1))].reshape((hidden_layer_size, 
                    input_layer_size + 1))

theta2 = nn_params[-((hidden_layer_size + 1) * 
                    num_labels):].reshape((num_labels,
                    hidden_layer_size + 1))

#Implement Predict

pred = predict(theta1,theta2,X)
acc = sum(list(map(lambda z: 1 if z[0] == z[1] else 0,zip(pred,y)))) / X.shape[0]
print('\nTraining Set Accuracy: {} %\n'.format(acc*100))