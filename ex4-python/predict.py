import numpy as np
from sigmoid import sigmoid

def predict(theta1,theta2,X):
    z2 = np.dot(X,theta1.T)
    a2 = sigmoid(z2)
    a2 = np.append(np.ones((a2.shape[0],1)),a2,axis=1)
    z3 = np.dot(a2,theta2.T)
    a3 = sigmoid(z3)

    pred = np.argmax(a3,axis=1) + 1
    return pred


