import numpy as np
import pandas as pd
from sigmoid import sigmoid
from sigmoidGrad import sigmoidGrad

def nnCostFunction(nn_params, input_layer_size, hidden_layer_size, num_labels, X, y, l):
    
    m = X.shape[0]
    yMatrix = np.zeros((m,num_labels))
    for i in range(0,m): yMatrix[i,y[i]-1] = 1
    
    theta1 = nn_params[0:(input_layer_size+1)*hidden_layer_size].reshape((hidden_layer_size,input_layer_size+1))
    theta2 = nn_params[(input_layer_size+1)*hidden_layer_size:].reshape((num_labels,hidden_layer_size+1))
    #Forward propagation
    a1 = X

    z2 = np.dot(X,theta1.T)
    a2 = sigmoid(z2)
    a2 = np.append(np.ones((a2.shape[0],1)),a2,axis=1)
    
    z3 = np.dot(a2,theta2.T)
    a3 = sigmoid(z3)
    J = -1*np.sum((np.log(a3)*yMatrix+np.log(1-a3)*(1-yMatrix))) / m + \
        (l/(2*m))*(np.sum(np.square(theta1[:,1:]))+np.sum(np.square(theta2[:,1:])))

    #Backpropagation

    d3 = a3 - yMatrix
    d2 = d3.dot(theta2[:,1:]) * sigmoidGrad(z2)

    D2= d3.T.dot(a2)
    D1 = d2.T.dot(a1)

    grad2 = (D2 / m) + (l/m)*theta2
    grad2[0] = grad2[0] - (l/m)*theta2[0]
    grad1 = (D1 / m) + (l/m)*theta1
    grad1[0] = grad1[0] - (l/m)*theta1[0]

    gradient = np.append(grad1.ravel(),grad2.ravel())


    return (J,gradient)

    