import numpy as np

def randomInitializeWeights(in_l,out_l):
    epsilon_init = 0.12
    theta = np.random.randint(-100,high=100,size=(out_l,in_l+1)) * (epsilon_init/100)
    return theta