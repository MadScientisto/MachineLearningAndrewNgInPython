import numpy as np
import matplotlib.pyplot as plt

def displayData(X,title=None):
    m = X.shape[0]
    n = X.shape[1]
    gd = int(np.sqrt(m))

    X = X.reshape((m,int(np.sqrt(n)),int(np.sqrt(n))))

    grid = np.ones((1,(X.shape[1]*gd)+2))
    for i in range(gd):
        row = np.ones((X.shape[1],1))
        for j in range(gd):
            row = np.append(row,X[i*10+j,:,:].T,axis=1)
        row = np.append(row,np.ones((X.shape[1],1)),axis=1)
        grid = np.append(grid,row,axis=0)
    grid = np.append(grid,np.ones((1,grid.shape[1])),axis=0)
    
    if title:
        plt.title(title)
    plt.imshow(grid,cmap='gray')
    plt.show()

    return




