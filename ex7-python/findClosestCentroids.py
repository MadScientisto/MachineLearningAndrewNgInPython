import numpy as np

def distance(x,c):
    dist = np.sum(np.square(x - c)) ** (1/2)
    return dist

def findClosestCentroids(X,centroids):
    m = X.shape[0]
    K = centroids.shape[0]
    
    distances = np.zeros((m,K))

    for i in range(m):
        for j in range(K):
            distances[i,j] = distance(X[i,:],centroids[j,:])

    return np.argsort(distances,axis=1)[:,0] + 1 
              