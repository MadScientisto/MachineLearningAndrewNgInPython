import matplotlib.pyplot as plt
from scipy.io import loadmat
from featureNormalize import featureNormalize
from pca import pca
from projectData import projectData
from recoverData import recoverData
from displayData import displayData


#Load Example Dataset
X = loadmat('ex7data1.mat')['X']

plt.scatter(X[:,0],X[:,1])
#plt.show()

#Principal Component Analysis
print('\nRunning PCA on example dataset.\n\n')

X_norm, mu, sigma = featureNormalize(X)
U, S = pca(X_norm)

print('Top eigenvector: \n')
print(' U[:,1] = {} {} \n'.format(U[0,0], U[1,0]))
print('\n(you should expect to see -0.707107 -0.707107)\n')

#Dimension Reduction

print('\nDimension reduction on example dataset.\n\n')
plt.scatter(X_norm[:,0],X_norm[:,1])
#plt.show()

K = 1
Z = projectData(X_norm, U, K)
print('Projection of the first example: {}\n'.format(Z[0]))
print('\n(this value should be about 1.481274)\n\n')

X_rec  = recoverData(Z, U, K)
print('Approximation of the first example: {} {}\n'.format(X_rec[0,0], X_rec[0,1]))
print('\n(this value should be about  -1.047419 -1.047419)\n\n')

#Loading and visualizing Face Data

X = loadmat('ex7faces.mat')['X']
displayData(X[0:100, :],title='Original Faces')

#PCA on Face Data: Eigenfaces

print('\nRunning PCA on face dataset.\n (this might take a minute or two ...)\n\n')

X_norm, mu, sigma = featureNormalize(X)
U, S = pca(X_norm)

#Dimension Reduction for Faces

K = 100
Z = projectData(X_norm, U, K)

print('The projected data Z has a size of: ')
print('{} '.format(Z.shape))

#Visualization of Faces after PCA Dimension Reduction

K = 100
X_rec  = recoverData(Z, U, K)

displayData(X_norm[0:100,:],title='Normalized faces')
displayData(X_rec[0:100,:],title='Recovered faces')


