import numpy as np
from scipy.io import loadmat
import matplotlib.pyplot as plt
from findClosestCentroids import findClosestCentroids
from computeCentroids import computeCentroids
from runkMeans import runkMeans
from kMeansInitCentroids import kMeansInitCentroids


#Find Closest Centroids

print('Finding closest centroids.\n\n')

data2 = loadmat('ex7data2.mat')
X2 = data2['X'] 
K = 3
initial_centroids = np.array([(3,3) ,(6,2), (8,5)])

idx = findClosestCentroids(X2, initial_centroids)

print('Closest centroids for the first 3 examples: \n')
print(' {}'.format(idx[0:3]))
print('\n(the closest centroids should be 1, 3, 2 respectively)\n')

#Compute Means

print('\nComputing centroids means.\n\n')

centroids = computeCentroids(X2, idx, K)
print(centroids)

print('Centroids computed after initial finding of closest centroids: \n')
#print(' {} {} \n'.format(centroids))
print('\n(the centroids should be\n')
print('   [ 2.428301 3.157924 ]\n')
print('   [ 5.813503 2.633656 ]\n')
print('   [ 7.119387 3.616684 ]\n\n')

#K-Means Clustering 

max_iters = 10
initial_centroids = np.array([[3,3], [6,2], [8,5]])

centroids, idx = runkMeans(X2, initial_centroids, max_iters, plot=True)
print(centroids)
print('\nK-Means Done.\n\n')

#K-Means Clustering on Pixels

print('\nRunning K-Means clustering on pixels from an image.\n\n')

A = loadmat('bird_small.mat')['A']
A = A / 255
A_shape = A.shape

A = A.reshape(A.shape[0]*A.shape[1],A.shape[2])
K = 16
max_iters = 10
initial_centroids = kMeansInitCentroids(A, K)
centroids, idx = runkMeans(A, initial_centroids, max_iters,plot=False)

#Image Compression

idx = findClosestCentroids(A,centroids)
A_recovered = centroids[idx-1].reshape((A_shape[0],A_shape[1],A_shape[2]))

plt.imshow(A_recovered)
plt.title("Image compressed with {} colors".format(K))
plt.show()