import numpy as np
import pandas as pd

def computeCentroids(X,idx,K):
    m = X.shape[0]
    n = X.shape[1]

    df = pd.DataFrame(np.append(X,idx.reshape(m,1),axis=1))
    df = df.groupby(n)
    df = {k:v for (k,v) in df}

    centroids = [np.array(df[float(k)].mean()[0:-1]) for k in range(1,K+1)]


    return np.array(centroids)