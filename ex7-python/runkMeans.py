import numpy as np
from computeCentroids import computeCentroids
from findClosestCentroids import findClosestCentroids
from plotMeans import plotMeans

def runkMeans(X, initial_centroids, max_iters, plot=False):
    K = initial_centroids.shape[0]
    centroids = initial_centroids
    cent_history = [centroids]

    for i in range(max_iters):
        idx = findClosestCentroids(X, centroids)
        centroids = computeCentroids(X, idx, K)
        cent_history.append(centroids)
        if plot:
            plotMeans(X,idx,K,cent_history)

    return centroids,idx
