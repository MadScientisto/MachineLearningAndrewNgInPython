import numpy as np
import matplotlib.pyplot as plt

def plotMeans(X,idx,K,cent_history):
    
    plt.scatter(X[:,0],X[:,1],c=idx,cmap='tab10')
    for i in range(K):
        c = np.array([frame[i] for frame in cent_history])
        plt.plot(c[:,0],c[:,1],'kx-',linewidth=1)
    plt.title('Iteration number {}'.format(len(cent_history)-1))
    plt.show()

    return
    
