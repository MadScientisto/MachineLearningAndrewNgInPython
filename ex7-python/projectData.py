import numpy as np

def projectData(X,U,K):
    U_reduce = U[:,0:K]
    project_X = X.dot(U_reduce)

    return project_X