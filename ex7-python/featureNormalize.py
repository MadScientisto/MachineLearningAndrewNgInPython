import numpy as np

def featureNormalize(X):
    mu = np.mean(X,axis=0)
    sigma = np.std(X,axis=0)
    norm_X = (X - mu) / sigma

    return norm_X,mu,sigma

