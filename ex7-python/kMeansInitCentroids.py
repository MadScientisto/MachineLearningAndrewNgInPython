import numpy as np

def kMeansInitCentroids(X,K):
    return np.random.permutation(X)[0:K,:]