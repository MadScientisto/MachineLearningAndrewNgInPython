import numpy as np
from scipy.optimize import minimize
from lrCostFunctionReg import lrCostFunctionReg
from lrGradReg import lrGradReg

def oneVsAll(X, y, num_labels, l):
    initial_theta = np.zeros((X.shape[1],1))
    all_theta = np.zeros((X.shape[1], num_labels))

    for i in range(0,num_labels):
        res = minimize(lrCostFunctionReg, initial_theta, args=(X, (y == i+1)*1, l),
                         method=None,jac=lrGradReg, options={'maxiter':50})
        all_theta[:,i] = res.x
    return(all_theta)
