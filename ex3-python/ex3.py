import numpy as np
import matplotlib.pyplot as plt
from scipy.io import loadmat
from lrCostFunctionReg import lrCostFunctionReg
from lrGradReg import lrGradReg
from oneVsAll import oneVsAll
from predictOneVsAll import predictOneVsAll

input_layer_size  = 400
num_labels = 10


#Part 1: Loading and Visualizing Data

print('Loading and Visualizing Data ...\n')

data = loadmat('ex3data1.mat')
X = np.append(np.ones((data["X"].shape[0],1)),data["X"],axis=1)
y = data["y"]

sample = np.random.choice(X.shape[0], 20)
plt.imshow(X[sample,1:].reshape(-1,20).T)
plt.axis('off')
plt.show()

#Part 2a: Vectorize Logistic Regression

print('\nTesting lrCostFunction() with regularization')

theta_t = np.array([-2, -1, 1, 2]).reshape((4,1))
X_t = np.array(np.ones((5,4)))
y_t = np.array([1,0,1,0,1]).reshape((5,1))
lambda_t = 3
J = lrCostFunctionReg(theta_t, X_t, y_t, lambda_t)
grad = lrGradReg(theta_t, X_t, y_t, lambda_t)

print('\nCost: {}\n'.format(J))
print('Expected cost: 2.534819\n')
print('Gradients:\n')
print(' {} \n'.format(grad))
print('Expected gradients:\n')
print(' 0.146561\n -0.548558\n 0.724722\n 1.398003\n')

#Part 2b: One-vs-All Training
print('\nTraining One-vs-All Logistic Regression...\n')

l = 0.1
all_theta = oneVsAll(X, y, num_labels, l)

#Part 3: Predict for One-Vs-All

pred = predictOneVsAll(all_theta, X)
acc = sum(list(map(lambda z: 1 if z[0] == z[1] else 0,zip(pred,y)))) / X.shape[0]
print('\nTraining Set Accuracy: {} %\n'.format(acc*100))



