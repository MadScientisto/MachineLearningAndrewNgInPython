import numpy as np
import matplotlib.pyplot as plt
from scipy.io import loadmat
from predict import predict

input_layer_size  = 400
hidden_layer_size = 25
num_labels = 10

#Part 1: Loading and Visualizing Data

print('Loading and Visualizing Data ...\n')

data = loadmat('ex3data1.mat')
X = np.append(np.ones((data["X"].shape[0],1)),data["X"],axis=1)
y = data["y"]

sample = np.random.choice(X.shape[0], 20)
plt.imshow(X[sample,1:].reshape(-1,20).T)
plt.axis('off')
plt.show()

#Part 2: Loading Pameters

weights = loadmat('ex3weights.mat')
theta1, theta2 = weights['Theta1'], weights['Theta2']

#Part 3: Implement Predict

pred = predict(theta1, theta2, X)
acc = acc = sum(list(map(lambda z: 1 if z[0] == z[1] else 0,zip(pred,y)))) / X.shape[0]
print('\nTraining Set Accuracy: {} %\n'.format(acc*100))
