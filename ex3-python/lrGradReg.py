import numpy as np 
from sigmoid import sigmoid

def lrGradReg(theta,X,y,l):
    m = len(y)
    theta = theta.reshape((X.shape[1]),1)
    
    h = sigmoid(np.dot(X,theta))
    error = np.subtract(h,y)
    grad = np.dot(X.T,error) / m

    regGrad = np.add(grad,theta*(l/m))
    regGrad[0,0] = grad[0,0]
    
    return regGrad.flatten()