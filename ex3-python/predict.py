import numpy as np
from sigmoid import sigmoid

def predict(theta1,theta2,X):
    theta1 = theta1.T
    theta2 = theta2.T

    print(X.shape,theta1.shape)
    a2 = np.append(np.ones((X.shape[0],1)),sigmoid(np.dot(X,theta1)),axis=1)
    print(a2.shape)
    a3 = np.dot(a2,theta2)
    print(a3.shape)
    pred = np.argmax(a3,axis=1) + 1

    return pred