import numpy as np
from sigmoid import sigmoid

def predictOneVsAll(theta,X):
    h = sigmoid(np.dot(X,theta))
    pred = np.argmax(h,axis=1) + 1

    return pred