import numpy as np
from sigmoid import sigmoid

def lrCostFunctionReg(theta,X,y,l):
    m = len(y)
    theta = theta.reshape((X.shape[1]),1)
    
    h = sigmoid(np.dot(X,theta))
    cost = (-np.dot(y.T,np.log(h))-np.dot((1-y.T),np.log(1-h)))/m
    regCost = cost + (l / (m * 2)) * sum(np.power(theta[1:,:],2))

    return regCost.flatten()  