import numpy as np

def cofiCostFunc(params,Y,R,num_users,num_movies,num_features,l):

    X = params[0:num_movies*num_features].reshape((num_movies,num_features))
    X = X.reshape((num_movies,num_features))
    Theta = params[num_movies*num_features:]
    Theta = Theta.reshape((num_users,num_features))

    J = 0.5 * np.sum(np.multiply(np.square((X.dot(Theta.T)) - Y),R)) + \
        (l/2)*np.sum(np.square(Theta)) + (l/2)*np.sum(np.square(Theta))
        
    gradX= (np.multiply((X.dot(Theta.T)-Y),R)).dot(Theta) + l*X
    gradTheta = (np.multiply((X.dot(Theta.T)-Y),R)).T.dot(X) + l*Theta
    grad = np.append(gradX.ravel(),gradTheta.ravel())
    
    return J,grad