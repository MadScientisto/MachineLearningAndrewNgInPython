import numpy as np

def multivariateGaussian(X,mu,sigma2):
    p = 1/np.sqrt(2*np.pi*sigma2) * np.exp(-((X-mu)**2)/(2*sigma2))
    p = np.prod(p,axis=1)
    return p