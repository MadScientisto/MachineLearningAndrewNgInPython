import numpy as np

def normalizeRatings(Y,R):
    
    Ymean = np.zeros((Y.shape[0],1))
    Ynorm = np.zeros(Y.shape)

    for i in range(len(Ymean)):
        Ymean[i] = np.sum(Y[i,:]) / np.sum(R[i,:])
        Ynorm[i,:] = np.multiply(Y[i,:] - Ymean[i],R[i,:])
    return Ynorm,Ymean