import numpy as np
import matplotlib.pyplot as plt
from multivariateGaussian import multivariateGaussian

def visualizeFit(X,mu,sigma2):
    plt.scatter(X[:,0],X[:,1],marker='x',color='b',linewidths=1)

    x1min, x1max = np.min(X[:,0]), np.max(X[:,0])
    x2min, x2max = np.min(X[:,1]), np.max(X[:,1])
    XX1,XX2 = np.meshgrid(np.linspace(x1min-5,x1max+5,num=100),np.linspace(x2min-5,x2max+5,num=100))
    XX = np.append(XX1.ravel().reshape((10000,1)),XX2.ravel().reshape((10000,1)),axis=1)
    P = multivariateGaussian(XX,mu,sigma2).reshape(XX1.shape)
    levels = np.array([10.0**i for i in np.arange(-20,0,3)])

    plt.contour(XX1,XX2,P,levels)
    plt.xlabel('Latency (ms)')
    plt.ylabel('Throughput (mb/s)')
    plt.show()

    return
    