import numpy as np
import matplotlib.pyplot as plt
from scipy.io import loadmat
from scipy.optimize import minimize
from cofiCostFunc import cofiCostFunc
from normalizeRatings import normalizeRatings

#Loading movie ratings dataset

data1 = loadmat('ex8_movies.mat')
Y = data1['Y']
R = data1['R']

toystory = np.mean(Y[0,R[0,:]==1])
print('Average rating for movie 1 (Toy Story): {} / 5\n\n'.format(toystory))

plt.imshow(Y)
plt.ylabel('Movies')
plt.xlabel('Users')
#plt.show()

#Collaborative Filtering Cost Function

data2 = loadmat('ex8_movieParams.mat')
X = data2['X']
Theta = data2['Theta']
num_users = data2['num_users']
num_movies = data2['num_movies']
num_features = data2['num_features']

num_users = 4
num_movies = 5
num_features = 3
X = X[0:num_movies,0:num_features]
Theta = Theta[0:num_users,0:num_features]
Y = Y[0:num_movies,0:num_users] 
R = R[0:num_movies,0:num_users]
params = np.append(X.flatten(),Theta.flatten())

J,grad = cofiCostFunc(params, Y, R, num_users, num_movies,num_features, 0)

print('Cost at loaded parameters: {} \
        \n(this value should be about 22.22)\n'.format(J))

#Collaborative Filtering Cost Regularization

J,grad = cofiCostFunc(params, Y, R, num_users, num_movies,num_features, 1.5)

print('Cost at loaded parameters (lambda = 1.5): {} \
        \n(this value should be about 31.34)\n'.format(J))

#Entering ratings for a new user

with open('movie_ids.txt','r') as movies:
    movieList = np.array([line.strip().split(' ',1) for line in movies.readlines()])

my_ratings = np.zeros((1682, 1))

my_ratings[7] = 3
my_ratings[12]= 5
my_ratings[54] = 4
my_ratings[64]= 5
my_ratings[66]= 3
my_ratings[69] = 5
my_ratings[183] = 4
my_ratings[226] = 5
my_ratings[355]= 5

for i in range (len(my_ratings)):
    if my_ratings[i] > 0:
        print('Rated {} for {}\n'.format(movieList[i,1],my_ratings[i]))

#Learning Movie Ratings

Y = data1['Y']
R = data1['R']

Y = np.append(my_ratings,Y,axis=1)
R = np.append(np.sign(my_ratings),R,axis=1)

Ynorm, Ymean = normalizeRatings(Y, R)

num_users = Y.shape[1]
num_movies = Y.shape[0]
num_features = 10

X = np.random.randn(num_movies,num_features)
Theta = np.random.randn(num_users, num_features)

init_params = np.append(X.flatten(),Theta.flatten())
res = minimize(cofiCostFunc,init_params, \
                args=(Ynorm,R,num_users, num_movies,num_features, 10), \
                method='CG',jac=True,options={'maxiter':100,'disp':False})
opt_params = res.x

X = opt_params[0:num_movies*num_features].reshape(X.shape)
Theta = opt_params[num_movies*num_features:].reshape(Theta.shape)

print('Recommender system learning completed.\n')

p = X.dot(Theta.T)
my_predictions = p[:,0].flatten() + Ymean.flatten()
top_idx = np.argsort(my_predictions)[::-1]

print('\nTop recommendations for you:\n')
for i in range(10):
    j = top_idx[i]
    print('Predicting rating {} for movie {}\n' \
        .format(round(my_predictions[j]),movieList[j]))

print('\n\nOriginal ratings provided:\n')
for i in range(len(my_ratings)):
    if my_ratings[i] > 0 :
        print('Rated {} for {}\n'.format(movieList[i][1],my_ratings[i]))