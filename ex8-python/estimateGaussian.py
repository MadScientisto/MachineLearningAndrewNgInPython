import numpy as np

def estimateGaussian(X):
    mu = np.mean(X,axis=0)
    sigma2 = np.var(X,axis=0)
    return mu,sigma2