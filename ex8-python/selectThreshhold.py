import numpy as np

def selectThreshhold(pval,yval):
    maxf1 = 0
    opt_e = 0 
    yval = yval.ravel()

    for e in np.linspace(np.min(pval),np.max(pval),1000):
 
        classify = np.vectorize(lambda p,e=e: 1 if p < e else 0 )
        pclass = classify(pval)
        pdict = {'tp':0,'tn':0,'fp':0,'fn':0}

        for i in range(yval.shape[0]):
            if pclass[i] == 0:
                if yval[i] == 0:
                    pdict['tn'] += 1
                else:
                    pdict['fn'] += 1
            else:
                if yval[i] == 1:
                    pdict['tp'] += 1
                else:
                    pdict['fp'] += 1

        if pdict['tp'] > 0:
            prec = pdict['tp'] / (pdict['tp'] + pdict['fp'])
            rec = pdict['tp'] / (pdict['tp'] + pdict['fn'])
            f1 = 2*prec*rec/(prec+rec)
        else:
            f1 = 0

        if f1 > maxf1:
            maxf1 = f1
            opt_e = e
    
    return opt_e, maxf1