import numpy as np
import matplotlib.pyplot as plt
from scipy.io import loadmat
from estimateGaussian import estimateGaussian
from multivariateGaussian import multivariateGaussian
from visualizeFit import visualizeFit
from selectThreshhold import selectThreshhold

#Load Example Dataset

data1 = loadmat('ex8data1.mat')
X = data1['X']
Xval = data1['Xval']
yval = data1['yval']

plt.scatter(X[:,0],X[:,1],marker='x',color='b',linewidths=1)
plt.xlabel('Latency (ms)')
plt.ylabel('Throughput (mb/s)')
plt.show()

#Estimate the dataset statistics

mu,sigma2 = estimateGaussian(X)

p = multivariateGaussian(X,mu,sigma2)

visualizeFit(X,  mu, sigma2)

pval = multivariateGaussian(Xval, mu, sigma2)

epsilon,f1 = selectThreshhold(pval,yval)
print('Best epsilon found using cross-validation: {}\n'.format(epsilon))
print('Best F1 on Cross Validation Set:  {}\n'.format(f1))
print('   (you should see a value epsilon of about 8.99e-05)\n')
print('   (you should see a Best F1 value of  0.875000)\n\n')

#Multidimensional Outliers

data2 = loadmat('ex8data2.mat')
X = data2['X']
Xval = data2['Xval']
yval = data2['yval']

mu, sigma2 = estimateGaussian(X)
p = multivariateGaussian(X,mu,sigma2)
pval = multivariateGaussian(Xval,mu,sigma2)

epsilon,f1 = selectThreshhold(pval,yval)

classify = np.vectorize(lambda p,e=epsilon: 1 if p < e else 0 )

print('Best epsilon found using cross-validation: {}\n'.format(epsilon))
print('Best F1 on Cross Validation Set:  {}\n'.format(f1))
print('   (you should see a value epsilon of about 1.38e-18)\n')
print('   (you should see a Best F1 value of 0.615385)\n')
print('# Outliers found: {}\n\n'.format(sum(classify(p))))